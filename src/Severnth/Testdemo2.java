package Severnth;

/**
 * User:langchen
 * Date:2022-08-11
 * Time:23:28
 */
class demo1 implements  Cloneable{
    public int id;

    @Override
    protected Object clone() throws CloneNotSupportedException {
        return super.clone();
    }

    @Override
    public String toString() {
        return "demo1{" +
                "id=" + id +
                '}';
    }
}
public class Testdemo2 {
    public static void main(String[] args)throws CloneNotSupportedException {
        demo1 d1 = new demo1();
        d1.id = 99;
        demo1 d2 = (demo1) d1.clone();
        System.out.println(d1);
        System.out.println(d2);
    }
}
