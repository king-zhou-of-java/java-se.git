package Severnth;

/**
 * User:langchen
 * Date:2022-08-11
 * Time:20:49
 */
interface Idemo1 {
    public abstract void way1();
}
interface Idemo2{
    void way2();
}
interface Idemo3 extends Idemo1,Idemo2{

}
class test1 implements Idemo1,Idemo2{
    @Override
    public void way1() {
        System.out.println("实现了第一个接口Idemo1");
    }

    @Override
    public void way2() {
        System.out.println("实现了第二个接口Idemo2");
    }
}
