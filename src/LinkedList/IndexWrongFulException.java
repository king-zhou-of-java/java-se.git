package LinkedList;

/**
 * User:
 * Date:2022-09-18
 * Time:16:39
 */
public class IndexWrongFulException extends RuntimeException{
    public IndexWrongFulException() {
    }

    public IndexWrongFulException(String message) {
        super(message);
    }
}
