import java.util.Arrays;

/**
 * User:langchen
 * Date:2022-08-01
 * Time:9:24
 */
public class Severnstudy {
    public static void func1(int[] arr){
        arr = new int[10];
    }
    public static void func2(int []arr){
        arr[0]=99;
    }
    public static void main(String[] args) {
        int[] arr= {1,2,3,4};//数组的定义初始化只有一次机会 不能在函数里面再次初始化数组
        String str = Arrays.toString(arr);
/*        func1(arr);
        func2(arr);*/
    }
}
