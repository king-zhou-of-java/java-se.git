/**
 * User:langchen
 * Date:2022-08-30
 * Time:22:15
 */
public class Testdemo2 {
    public static int getElement(int[] array, int index){
        if(null == array){

            throw new NullPointerException("传递的参数为null");
            }

        if(index < 0 || index >= array.length){
            throw new ArrayIndexOutOfBoundsException("传递的数组下标越界");
    }
            return array[index];
    }
    public static void main(String[] args) {
        int[] array = {1,2,3};
        getElement(array, 3);
    }
}
