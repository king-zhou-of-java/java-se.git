import java.util.Arrays;

/**
 * User:langchen
 * Date:2022-08-02
 * Time:9:37
 */
class Dog{
    public String name;
    public String color;
}
public class Erweiarray {
    public static void main(String[] args) {
        int[][] arr = new int[][]{{1,2,3},{4,5,6}};
        int[][] arr1 = new int[2][];//Java二维数组可以省略列  不能省略行
        arr1[0] = new int[3];//可以指定二维数组的列
        arr1[1] = new int[4];
        System.out.println(arr[0]);
        System.out.println(arr[1]);
        System.out.println(Arrays.deepToString(arr));//最快的打印方式
/*        for (int i = 0; i < 2; i++) {
            for (int j = 0; j < 3; j++) {
                System.out.println(arr[i][j]);
            }
        }
        System.out.println(Arrays.toString(arr));*/
    }
}
