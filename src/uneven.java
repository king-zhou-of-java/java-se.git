import java.util.Arrays;

/**
 * User:langchen
 * Date:2022-08-01
 * Time:19:53
 */
//调整数组顺序使得奇数位于偶数之前。调整之后，不关心大小顺序。
//如数组：[1,2,3,4,5,6]
//调整后可能是：[1, 5, 3, 4, 2, 6]
public class uneven {
    public static void even(int[] arr){
        for (int i = 0; i < arr.length; i++) {
            for (int j = i+1; j < arr.length; j++) {
                if(arr[i]%2==0){
                    int tmp = arr[j];
                    arr[j] = arr[i];
                    arr[i] =tmp;
                }
            }
            System.out.print(arr[i]+" ");
        }
    }
    public static void main(String[] args) {
        int[] arr= {1,2,3,4,5,6};
        System.out.println("调整前"+Arrays.toString(arr));
        even(arr);
    }
}
