package Thirteen;

/**
 * User:langchen
 * Date:2022-08-07
 * Time:16:07
 */
public class Animal { //父类
        public String name;
        public String color;
        public int age;
        public void eat(){
            System.out.println(name+"吃饭");
        }
        public void drink(){
            System.out.println(name+"喝水");
        }
    }
 class Dog extends Animal{//子类1
        public void bark(){
            System.out.println("汪汪~");
        }
    }
class Cat extends Animal{//子类2
        public void mew(){
            System.out.println(name+"喵喵~");
        }
    }

