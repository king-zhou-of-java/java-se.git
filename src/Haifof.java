/**
 * User:langchen
 * Date:2022-08-01
 * Time:20:15
 */
//给定一个有序整型数组, 实现二分查找
public class Haifof {
    public static int binarySearch(int[] arr, int toFind) {
        int left = 0;
        int right = arr.length - 1;
        while (left <= right) {
            int mid = (left + right) / 2;
            if (toFind < arr[mid]) {
                right = mid - 1;
            } else if (toFind > arr[mid]) {
                left = mid + 1;
            } else {
                return mid;
            }
        }
        return -1;
    }

    public static void main(String[] args) {
        int[] arr = {1, 2, 3, 4, 5, 6, 7, 8};
        int toFind = 7;
        int ret = binarySearch(arr, toFind);
        System.out.println(ret);
    }
}
