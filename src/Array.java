/**
 * User:langchen
 * Date:2022-07-31
 * Time:16:05
 */
//创建一个 int 类型的数组, 元素个数为 100, 并把每个元素依次设置为 1 - 100
//方法3
//实现一个方法 printArray, 以数组为参数, 循环访问数组中的每个元素, 打印每个元素的值.
public class Array {
    public static void printArray(int[] arr){
        for (int x:arr) {
            System.out.println(x);
      }
    }
    public static void main(String[] args) {
        int[] arr1= new int[]{1,2,3,4,5,6,7,8,9};
        int[] array = new int[10];
        int[] arr = new int[100];
        int[] arr2 = {1,32,34,4,4,4};
        for (int i = 0; i < arr.length; i++) {
            arr[i]= i+1;
        }
        printArray(arr);
    }
}
