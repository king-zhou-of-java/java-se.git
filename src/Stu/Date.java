package Stu;

/**
 * User:langchen
 * Date:2022-08-05
 * Time:15:28
 */
public class Date {
    public int year;
    public int month;
    public int day;

    public void setDate(int year, int month, int day) {
        this.year = year;
        this.month = month;
        this.day = day;
    }
    static {
        System.out.println("静态代码块执行");
    }

    //构造方法
    // 名字与类名相同，没有返回值类型，设置为void也不行
    // 一般情况下使用public修饰
    // 在创建对象时由编译器自动调用，并且在对象的生命周期内只调用一次
    //构造方法的作用就是对对象中的成员进行初始化，并不负责给对象开辟空间
    public Date(int year, int month, int day) {
        this.year = year;
        this.month = month;
        this.day = day;
        System.out.println("Date(int,int,int)方法被调用了");
    }
    public Date(){
        //此处可以在无参构造方法中通过this调用带有三个参数的构造方法
        //但是this(1900,1,1);必须是构造方法中第一条语句
        this(2022,8,8);
    }

    //this是“成员方法”第一个隐藏的参数，编译器会自动传递，在成员方法执行时，
    // 编译器会负责将调用成员方法,对象的引用传递给该成员方法，this负责来接收
    public void printfDate(Date this) {

        System.out.println(this.year + "/" + this.month + "/" + this.day);
    }
    /*public void printDate(){
        System.out.println(year+"/"+month+"/"+day);
    }*/
    public static void main(String[] args) {
        Date date = new Date(2022,8,6);
        date.setDate(2022,8,5);
        //date.printDate();
    }
}
