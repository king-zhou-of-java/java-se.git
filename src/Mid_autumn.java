import javax.swing.*;

/**
 * User:langchen
 * Date:2022-09-10
 * Time:9:41
 */
import javax.swing.*;
import java.awt.*;
import java.awt.event.*;

public class Mid_autumn extends JFrame {
    public static void main(String[] args){
        JFrame jf =new JFrame();
        jf.setTitle("美女专属~");
        jf.setSize(1024,768);
        jf.setBackground(Color.BLACK);
        jf.setLocationRelativeTo(null);

        MyJPanel mj =new MyJPanel();
        jf.add(mj);

        jf.addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent e){
                System.exit(0);
            }
        });
        jf.setVisible(true);
    }
}
class MyJPanel extends JPanel{
    public void paint(Graphics g){
        //制作星星，随机生成
        g.setColor(Color.WHITE);
        for(int i=0;i<360;i++){
            g.drawString("*", (int)(Math.random()*1024), (int)(Math.random()*768));
        }

        g.setColor(Color.YELLOW);
        g.fillOval(200, 100, 100, 100);


        g.setColor(Color.WHITE);
        g.setFont(new Font("楷体",Font.BOLD,30));
        g.setColor(Color.YELLOW);
        g.drawString("俺滴小公主，中秋节快乐哟~",500,100);
        g.setColor(Color.YELLOW);
        g.drawString("开开心心，可可爱爱每一天~",500,200);
        g.setColor(Color.YELLOW);
        g.drawString("满目星辰不及你 love油 嘿嘿~",500,300);
    }
}
