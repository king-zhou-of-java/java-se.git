package GetException;

/**
 * User:
 * Date:2022-09-01
 * Time:20:19
 */
public class UserNameException extends RuntimeException{
    public UserNameException(String message){
        super(message);
    }
}
