package GetException;

/**
 * User:
 * Date:2022-09-01
 * Time:20:20
 */
public class PasswordException extends RuntimeException{
    public PasswordException(String message){
        super(message);
    }
}
