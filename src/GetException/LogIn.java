package GetException;

/**
 * User:langchen
 * Date:2022-09-01
 * Time:20:16
 */
public class LogIn {
    private String userName = "lingchenqi";
    private String password = "123456";

    public void loginInfo(String userName, String password)
            throws UserNameException, PasswordException {
        try {
            if (!this.userName.equals(userName)) {
                throw new UserNameException("用户名错误！");
            }
            if (!this.password.equals(password)) {
                throw new PasswordException("密码错误！");
            }
            System.out.println("登陆成功");
        } catch (UserNameException e) {
            e.printStackTrace();
        } catch (PasswordException e) {
            e.printStackTrace();
        }
    }

    public static void main(String[] args) {
        LogIn login = new LogIn();
        login.loginInfo("lingchenqi","66666");
    }
}
