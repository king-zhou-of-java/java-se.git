package TestDemo1;

/**
 * User:langchen
 * Date:2022-08-10
 * Time:19:57
 */
public class TestString {
    public static void main(String[] args) {
        String s1 = new String("hello");
        String s2 = new String("hello");
        String s3 = new String("Hello");
        // s1、s2、s3引用的是三个不同对象，因此==比较结果全部为false
        System.out.println(s1 == s2); // false
        System.out.println(s1 == s3); // false
// equals比较：String对象中的逐个字符
// 虽然s1与s2引用的不是同一个对象，但是两个对象中放置的内容相同，因此输出true
// s1与s3引用的不是同一个对象，而且两个对象中内容也不同，因此输出false
        System.out.println(s1.equals(s2)); // true
        System.out.println(s1.equalsIgnoreCase(s3)); // false
/*        int a = 10;
        int b = 20;
        int c = 10;
// 对于基本类型变量，==比较两个变量中存储的值是否相同
    System.out.println(a == b); // false
    System.out.println(a == c); // true
    // 对于引用类型变量，==比较两个引用变量引用的是否为同一个对象
    String s1 = new String("hello");
    String s2 = new String("hello");
    String s3 = new String("world");
    String s4 = s1;
    System.out.println(s1 == s2); // false
    System.out.println(s2 == s3); // false
    System.out.println(s1 == s4); // true*/
        // s1和s2引用的是不同对象 s1和s3引用的是同一对象
/*        String s1 = new String("hello");
        String s2 = new String("world");
        String s3 = s1;

        System.out.println(s1.length()); // 获取字符串长度---输出5
        System.out.println(s1.isEmpty()); // 如果字符串长度为0，返回true，否则返回false*/
    }
}
