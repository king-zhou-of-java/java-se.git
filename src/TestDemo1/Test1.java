package TestDemo1;

import java.util.Arrays;

/**
 * User:langchen
 * Date:2022-08-10
 * Time:15:44
 */
class Student implements Comparable<Student>{//让Student类实现 Comparable 接口
    public String name;
    public int age;

    public Student(String name, int age) {
        this.name = name;
        this.age = age;
    }

    @Override
    public String toString() {
        return "Student{" +
                "name='" + name + '\'' +
                ", age=" + age +
                '}';
    }

    @Override
    public int compareTo(Student o){//在这个接口中实现compare To 方法
        if(this.age - o.age > 0){
            return 1;
        }else if(this.age - o.age < 0){
            return -1;
        }
        else{
            return 0;
        }

    }
}
public class Test1 {
    public static void main(String[] args) {
       Student[] stu  = new Student[3];
       stu[0] = new Student("xiaohu",18);
       stu[1] = new Student("zhangsan",19);
       stu[2] = new Student("xiaoming",20);
       Arrays.sort(stu);
        System.out.println(Arrays.toString(stu));
    }
}
