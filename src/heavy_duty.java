/**
 * User:langchen
 * Date:2022-07-29
 * Time:20:29
 */
//在同一个类中,分别定义求两个整数的方法 和 三个小数之和的方法。 并执行代码，求出结果
public class heavy_duty {
    public static int heavy(int x,int y){
        return x+y;
    }
    public static double heavy(double x,double y,double z){
        return x+y+z;
    }
    public static void main(String[] args) {
        int a = 1, b = 2;
        double c = 1.1 ,d = 1.2, e = 1.3;
        System.out.println(heavy(a,b));
        System.out.println(heavy(c,d,e));
    }
}
