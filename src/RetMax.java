/**
 * User:langchen
 * Date:2022-07-29
 * Time:20:42
 */
//在同一个类中定义多个方法：要求不仅可以求2个整数的最大值，还可以求3个小数的最大值？
public class RetMax {
    public static int max(int x,int y){
        return x>y?x:y;
    }
    public static double max1(double x,double y,double z){
        double Max = x;
        if(x<y) {
            Max = y;
        }
        if(x<z){
            Max = z;
        }
        if(z<y){
            Max = y;
        }
        return Max;
    }
    public static void main(String[] args) {
        int a = 1,b= 2;
        double c = 1.1, d = 1.2,e = 1.3;
        System.out.println(max(a,b));
        System.out.println(max1(c,d,e));
    }
}
