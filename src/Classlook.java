/**
 * User:langchen
 * Date:2022-08-04
 * Time:9:13
 */
public class Classlook {
    public int year;
    public int month;
    public int day;

    public Classlook(int year,int month,int day){
        this.year = year;
        this.month = month;
       this.day = day;
        System.out.println("Classlook()方法被调用了");
    }
    public void printDate(){
        System.out.println(year+"-"+month+"-"+day);
    }
    public static void main(String[] args) {
        Classlook s= new Classlook(2022,8,04);
        s.printDate();
    }
}
