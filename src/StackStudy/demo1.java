package StackStudy;

import java.util.Stack;

/**
 * User:浪尘
 * Date:2022-09-21
 * Time:21:15
 */
public class demo1 {
    public static void main(String[] args) {
        Stack<Integer> stack = new Stack<>();
        stack.push(12);
        stack.push(23);
        stack.push(34);
        int x = stack.pop();
        int y = stack.peek();
        int len = stack.size();
        boolean b1 = stack.empty();
        System.out.println(b1);
        System.out.println(len);
        System.out.println(x);
        System.out.println(y);
    }
}
