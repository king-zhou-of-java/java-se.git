import java.util.Scanner;

/**
 * User:langchen
 * Date:2022-07-29
 * Time:19:54
 */
//求N的阶乘
public class fac {
    public static int factorial(int x){
        if(x<2){
            return 1;
        }
        else{
           return x * factorial(x-1);
        }
    }
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int n = sc.nextInt();
        System.out.println(factorial(n));
    }
}
