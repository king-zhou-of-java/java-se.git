/**
 * User:langchen
 * Date:2022-07-27
 * Time:15:29
 */
public class demof {
    public static void main(String[] args) {
     /*   int a = 10;
        int b = 20;
        a > b? a : b;*/
        // 求两个整数的最小值
//        int a = 1;
//        int b = 22;
//        int max = a<b ? a : b;
//        System.out.println(max);
//        int a = 0xffffffff;
//        System.out.printf("%x\n", a >>> 1);
      /*  int a = 0x110;
        System.out.printf("%x\n", a >> 1);*/
       /* System.out.println(10 > 20 && 10 / 0 == 0); // 打印 false
        System.out.println(10 < 20 || 10 / 0 == 0); // 打印 true*/
        /*int a = 1;
        System.out.println(!(a == 1)); // a == 1 为true，取个非就是false
        System.out.println(!(a != 1));// a != 1 为false，取个非就是true*/
    /*    int a = 1;
        int b = 2;
        System.out.println(a == 1 || b == 2); // 左为真 且 右为真 则结果为真
        System.out.println(a == 1 || b > 100); // 左为真 但 右为假 则结果也为真
        System.out.println(a > 100 || b == 2); // 左为假 但 右为真 则结果也为真
        System.out.println(a > 100 || b > 100); // 左为假 且 右为假 则结果为假*/
     /*   int a = 1;
        int b = 2;
        System.out.println(a == 1 && b == 2);
        System.out.println(a == 1 && b > 100);
        System.out.println(a > 100 && b == 2);
        System.out.println(a > 100 && b > 100);*/
/*        int a = 1;
        int b = 2;
// 注意：在Java中 = 表示赋值，要与数学中的含义区分
//   在Java中 == 表示相等
        System.out.println(a == b);
        System.out.println(a != b);
        System.out.println(a < b);
        System.out.println(a > b);
        System.out.println(a <= b);
        System.out.println(a >= b);*/
/*        int a = 1;
        a++;   // 后置++  表示给a的值加1，此时a的值为2
        System.out.println(a++);  // 注意：后置++是先使用变量原来值，表示式结束时给变量+1，因此输出2
        System.out.println(a);   // 输出3
        ++a;   // 前置++  表示给a的值加1
        System.out.println(++a);  // 注意：前置++是先给变量+1，然后使用变量中的值，因此输出5
        System.out.println(a);   // 输出5*/
      /*  int a = 1;
        a += 2;          // 相当于 a = a + 2
        System.out.println(a);  // 输出3
        a -= 1;          // 相当于 a = a - 1
        System.out.println(a);  // 输出2
        a *= 3;          // 相当于 a = a * 3
        System.out.println(a);  // 输出6*/
       /* int a = 20;
        int b = 10;
        System.out.println(a + b); // 30
        System.out.println(a - b); // 10
        System.out.println(a * b); // 200
        System.out.println(a / b); // 2
        System.out.println(a % b); // 0 --->模运算相当于数学中除法的余数*/
    }
}
