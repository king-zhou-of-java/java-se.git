package Exercise;

import java.util.Scanner;

/**
 * User:langchen
 * Date:2022-09-02
 * Time:16:57
 */
public class huiwen {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        String str = sc.nextLine();
        StringBuilder str1 = new StringBuilder();
        int len = str.length();
        for (int i = 0; i < len; i++) {
            char ch = str.charAt(i);
            //判断字符是否为字母字符和数字字符
            if(Character.isLetterOrDigit(ch)){
                str1.append(Character.toLowerCase(ch));
            }
        }
        StringBuffer str2 = new StringBuffer(str1).reverse();
        if(str1.toString().equals(str2.toString())){
            System.out.println("yes");
        }
        else{
            System.out.println("no");
        }
    }
}
