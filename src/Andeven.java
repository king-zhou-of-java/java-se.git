/**
 * User:langchen
 * Date:2022-08-01
 * Time:21:57
 */
/*给你一个整数数组 arr，请你判断数组中是否存在连续三个元素都是奇数的情况：如果存在，请返回 true ；否则，返回 false 。
示例 1：
输入：arr = [2,6,4,1]
输出：false
解释：不存在连续三个元素都是奇数的情况。*/

public class Andeven {
    //方法2(简单好理解）
    public static boolean And2(int[] arr){
        int count = 0;
        for (int i = 0; i < arr.length; i++) {
            if(arr[i]%2!=0){
                count++;
                if(count==3){
                    return true;
                }
            }
            else{
                count = 0;
            }

        }
        return false;
    }

//方法1
/*    public static boolean And1(int[] arr) {
        //本题思路就是当判断第一个数为奇数时，接着判断下一个奇数的下标与上一次的奇数下标相减是否为1 来判断是否连续
        //若最后count等于3 return true
        int count =0;
        int index = -1;
        for (int i = 0; i < arr.length; i++) {
            if(arr[i]%2!=0){
                if(index==-1){
                    count++;
                }
                else if(i-index == 1){
                    count++;
                }
                else{
                    count = 1;
                    index = i;
                }
                if(count==3){
                    return true;
                }
            }
        }
        return false;
    }*/
    public static void main(String[] args) {
        int[] arr = {1,3,5,4,6,7};
        boolean ret = And2(arr);
        System.out.println(ret);
    }
}
