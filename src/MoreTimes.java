import java.util.Arrays;

/**
 * User:langchen
 * Date:2022-08-01
 * Time:21:31
 */
/*给定一个大小为 n 的数组，找到其中的多数元素。多数元素是指在数组中出现次数 大于 ⌊ n/2 ⌋ 的元素。
你可以假设数组是非空的，并且给定的数组总是存在多数元素。
示例 1：
输入：[3,2,3]
输出：3
*/
public class MoreTimes {
    public static void main(String[] args) {
        int[] arr = {2,3,4,5,2,3,4,5,6,5,5,5,5};
        Arrays.sort(arr);//先给数组排好序
        System.out.println(Arrays.toString(arr));//输出排序后的数组
        //因为要找大于n/2的元素 我们排好序后去中间的那个元素就是我们要找的元素
        System.out.println(arr[arr.length/2]);

    }
}
//摩尔投票法
/*
  public static int Times(int[]arr){
        for (int i = 0; i < arr.length; i++) {
            int sum =arr[i];
            int count = 0;
            if(sum==arr[i]){
                count++;
            }
            else{
                count--;
            }
            if(count==0){
              sum =  arr[i+1];
            }
            return ret;
        }
    }*/
