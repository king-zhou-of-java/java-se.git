package ArrayList;

import java.util.ArrayList;

/**
 * User:浪尘
 * Date:2022-09-15
 * Time:16:30
 */
public class demo3 {
    public static void main(String[] args) {
        // 构造一个空的列表
        //当我们调用不带参数的构造方法的时候，只有第一次add的时候 才会分配大小为10的内存
        ArrayList<String> s1 = new ArrayList<>();
        s1.add("hello");
        s1.add("java");
        System.out.println(s1);
        //构造一个具有3个容量的顺序表
        ArrayList<Integer> n1 = new ArrayList<>(3);
        n1.add(666);
        n1.add(888);
        n1.add(999);
        System.out.println(n1);
        //ArrayList(Collection<? extends E> c) 大类实现Collection接口便可以传
        ArrayList<Integer> n2 = new ArrayList<>(n1);//把n1里面的元素放到n2里面
        System.out.println(n2);
    }
}
