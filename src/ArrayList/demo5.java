package ArrayList;

import java.util.ArrayList;
import java.util.List;

/**
 * User:
 * Date:2022-09-19
 * Time:16:57
 */
public class demo5 {
    public static void main(String[] args) {
        List<String> list = new ArrayList<>();
        list.add("hello java");
        list.add("BATJ");
        list.add("大厂!!!");
        System.out.println(list);

        // 获取list中有效元素个数
        System.out.println(list.size());

        // 获取和设置index位置上的元素，注意index必须介于[0, size)间
        System.out.println(list.get(1));
        list.set(1, "好好学习！！！");
        System.out.println(list.get(1));

        // 在list的index位置插入指定元素，index及后续的元素统一往后搬移一个位置
        list.add(1, "学好数据结构！");
        System.out.println(list);

        // 删除指定元素，找到了就删除，该元素之后的元素统一往前搬移一个位置
        list.remove("BATJ");
        System.out.println(list);


        // 删除list中index位置上的元素，注意index不要超过list中有效元素个数,否则会抛出下标越界异常
        list.remove(list.size()-1);
        System.out.println(list);

        // 检测list中是否包含指定元素，包含返回true，否则返回false
        if(list.contains("学好数据结构！")){
        list.add("学好算法！");
        }

        // 使用list中[0, 4)之间的元素构成一个新的ArrayList返回
        List<String> ret = list.subList(0, 4);
        System.out.println(ret);

        list.clear();
        System.out.println(list.size());
    }
}
