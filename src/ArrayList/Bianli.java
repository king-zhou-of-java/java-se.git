package ArrayList;

import java.util.ArrayList;
import java.util.Iterator;

/**
 * User:浪尘
 * Date:2022-09-15
 * Time:17:10
 */
//三种遍历方式
public class Bianli {
    public static void main(String[] args) {
        ArrayList<Integer> c1 = new ArrayList<>();
        c1.add(1);
        c1.add(2);
        c1.add(3);
        c1.add(4);
        c1.add(5);
        for (int i = 0; i < c1.size(); i++) {//使用下标for遍历
            System.out.print(c1.get(i)+" ");
        }
        System.out.println();


        for (Integer s1:c1) {//使用foreach遍历
            System.out.print(s1+" ");
        }
        System.out.println();


        Iterator<Integer> it = c1.iterator();//使用迭代器遍历
        while (it.hasNext()){
            System.out.print(it.next()+" ");
        }
        System.out.println();
    }
}
