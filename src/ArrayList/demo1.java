package ArrayList;

import java.util.ArrayList;

/**
 * User:langchen
 * Date:2022-09-14
 * Time:18:49
 */
class Student {
    public String name;
    public int age;
    public int sco;

    public Student(String name, int age, int sco) {
        this.name = name;
        this.age = age;
        this.sco = sco;
    }

    @Override
    public String toString() {
        return "Student{" +
                "name='" + name + '\'' +
                ", age=" + age +
                ", sco=" + sco +
                '}';
    }
}
public class demo1 {
    public static void main(String[] args) {
        ArrayList<Student> stu = new ArrayList<Student>(3);
        stu.add(new Student("小明",18,88));
        stu.add(new Student("小红",19,89));
        stu.add(new Student("小白",20,78));
        for (Student stu1:stu) {
            System.out.println(stu1);
        }
    }
}
