import java.util.Arrays;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.zip.CheckedOutputStream;

/**
 * User:langchen
 * Date:2022-08-01
 * Time:14:59
 */
public class MytoString {
 /*   public static String my_String(int[] arr){
        //TODO:自己做一下assert断言 查一下怎么使用
        if(arr == null){
            return null;
        }
        String ret = "]";
        for (int i = 0; i < arr.length; i++) {
            ret+=arr[i];
            if(i!=arr.length){
                ret+=",";
            }
        }
        ret+="]";
        return ret;
    }*/
    public static void main(String[] args) {
        int[]arr = {1,2,3,4,5,7,45,34,67,46,98};
        Arrays.sort(arr);//直接将数组排序好（默认是升序）
        //降序需要给比较器 现在用不起来 知识储备不够
        int[] copy = new int[arr.length];
        int[] arr3= arr.clone();//克隆
        System.arraycopy(arr,0,copy,0,arr.length);
        int[] copy1 = Arrays.copyOfRange(arr,2,4);
        int[] arr2= Arrays.copyOf(arr,2*arr.length);//常用
        System.out.println(Arrays.toString(arr));
        System.out.println(Arrays.toString(copy));
        System.out.println(Arrays.toString(copy1));
//        System.out.println(Arrays.toString(arr2));
    }
}
