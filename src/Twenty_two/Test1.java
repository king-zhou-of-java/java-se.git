package Twenty_two;

import java.util.Locale;

/**
 * User:langchen
 * Date:2022-08-24
 * Time:19:19
 */
public class Test1 {
    public static void main(String[] args) {
        String str = "    hello java    ";
        System.out.println(str.trim());
//        String str = "192.168.1.1" ;
//       String[] result = str.split("\\.") ;
//       for(String s: result) {
//       System.out.println(s);
//       }
//       String s1 = "hello java get good work";
//       String[] arr = s1.split(" ",2);//按照空格拆分
//       for (String s:arr) {
//           System.out.println(s);
//        }
//        String str = "hello java";
//       System.out.println(str.replace("l","6"));
//       System.out.println(str.replaceFirst("l","J"));
//       String s1 = "hello JAVA";
//       char[] arr = s1.toCharArray();
//        for (int i = 0; i < arr.length; i++) {
//           System.out.print(arr[i]);
//        }
//       //数组转字符串
//       String s2 = new String(arr);
//       System.out.println(s2);
//        System.out.println();
//        String s1= "hello JAVA";
//        String s2 = "HAI google";
//       System.out.println(s1.toUpperCase());
//       System.out.println(s2.toLowerCase());
//        int num1 = Integer.parseInt("123");
//       double num2 = Double.parseDouble("123.45");
//       System.out.println(num1);
//       System.out.println(num2);
//        }
//       String s1 = String.valueOf(1234);
//        String s2 = String.valueOf(12.34);
//       String s3 = String.valueOf(true);
//        System.out.println(s1);
//        System.out.println(s2);
//
//       System.out.println(s3);
//       String s1 = new String("abc");
//        String s2 = new String("ac");
//       String s3 = new String("abc");
//       String s4 = new String("abcdef");


//        System.out.println(s1.compareTo(s2)); // 不同输出字符差值-1
//        System.out.println(s1.compareTo(s3)); // 相同输出 0
//        System.out.println(s1.compareTo(s4)); // 前k个字符完全相同，输出长度差值 -3
//        String s1 = new String("hello java");
//        String s2 = new String("hello java");
//        String s3 = new String("party");
//        System.out.println(s1.equals(s2));
//        System.out.println(s1.equals(s3));
        // 对于基本类型变量，==比较两个变量中存储的值是否相同
//        int a = 10;
//        int b = 20;
//        int c = 10;
//        System.out.println(a==b);
//        System.out.println(a==c);

//        // 使用常量串构造
//        String s1 = "hello java";
//        System.out.println(s1);
//
//        // 直接newString对象
//        String s2 = new String("hai java");
//        System.out.println(s2);
//
//        // 使用字符数组进行构造
//        char[] arr = {'h','e','l','l','o'};
//        String s3 = new String(arr);
//        System.out.println(s3);
    }
}
