import jdk.swing.interop.SwingInterOpUtils;

/**
 * User:langchen
 * Date:2022-07-31
 * Time:11:39
 */
//汉诺塔问题求解
/**
 * pos1:起始位置
 * pos2:跳板位置
 * pos3:目标位置
 */
public class Hanio {
    public static void han(int n,char pos1,char pos2,char pos3){
        if(n==1){
            move(pos1,pos3);
            return;
        }
        han(n-1,pos1,pos3,pos2);
        move(pos1,pos3);
        han(n-1,pos2,pos1,pos3);
    }
    public static void move(char pos1,char pos2){

        System.out.println(pos1+"->"+pos2);
    }
    public static void main(String[] args) {

        han(3,'A','B','C');
    }
}
