package EighteenStudy;
import java.util.Arrays;
import java.util.Locale;

/**
 * User:langchen
 * Date:2022-08-13
 * Time:15:27
 */
public class demo2 {
    public static void main(String[] args) {
//      但是这种方式不推荐使用，因为其效率非常低，中间创建了好多临时对象。
        String s = "hello";
        s += " world";
        System.out.println(s); // 输出：hello world
//        final int array[] = {1,2,3,4,5};
//        array[0] = 100;
//        System.out.println(Arrays.toString(array));
//        trim去除字符串的左右空格，保留中间空格
//        String str = " hello world ";
//        System.out.println("["+str+"]");
//        System.out.println("["+str.trim()+"]");
//        字符串截取
//        String s1 = "go to tencent baidu zijie";从指定索引截取到结尾
//        System.out.println(s1.substring(6));
//        System.out.println(s1.substring(0,5));截取部分内容



//        字符串拆分
//        String s1 = "hello java to_tencent baidu zijie";
//        String[] arr1 = s1.split(" |_");
//        for (String ret1:arr1) {
//            System.out.println(ret1);
//        }
//        String[] arr2 = s1.split(" ",2);
//        for (String ret2:arr2) {
//            System.out.println(ret2);
//        }


//        字符串替换
//        注意事项: 由于字符串是不可变对象, 替换不修改当前字符串, 而是产生一个新的字符串.
//        String s1 = "hellobit";
//        System.out.println(s1.replaceAll("l","6"));
//        System.out.println(s1.replaceFirst("o","8"));


//        格式化
//        String s = String.format("%d-%d-%d",2022,8,13);
//        System.out.println(s);

//        //字符串转数组
//        String s1 = "hello";
//        char[] arr = s1.toCharArray();
//        for (int i = 0; i < arr.length; i++) {
//            System.out.print(arr[i]);
//        }
//        System.out.println();
//        //数组转字符串
//        String s2 = new String(arr);
//        System.out.println(s2);


//       整数转字符串
//       String s1 = String.valueOf(1234);
//       String s2 = String.valueOf(123.5);
//        System.out.println(s1);
//        System.out.println(s2);



//        //字符串转数字
//        int date1 = Integer.parseInt("1234");
//        double date2 = Double.parseDouble("12.34");
//        int date3 = Integer.parseInt(s1);
//        System.out.println(date1);
//        System.out.println(date2);



//        大小写转换
//        String s1 = "hello";
//        String s2 = "HELLO";
//        System.out.println(s1.toUpperCase());
//        System.out.println(s2.toLowerCase(Locale.ROOT));
    }
}
