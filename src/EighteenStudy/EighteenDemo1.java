package EighteenStudy;

/**
 * User:langchen
 * Date:2022-08-13
 * Time:15:19
 */
public class EighteenDemo1 {
    public static void main(String[] args) {
        String s = "abctabcebacdefgh";
        System.out.println(s.charAt(3));//t
        System.out.println(s.indexOf('t'));//3
        System.out.println(s.indexOf('t',3));///3
        System.out.println(s.indexOf("abc",3));//4
        System.out.println(s.lastIndexOf('t'));//3
        System.out.println(s.lastIndexOf('c',3));//2
        System.out.println(s.lastIndexOf("abc",5));//4
    }
}
