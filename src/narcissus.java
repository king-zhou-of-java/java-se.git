/**
 * User:langchen
 * Date:2022-07-28
 * Time:15:25
 */
public class narcissus {
    public static void main(String[] args) {
        //求出0～n之间的所有“水仙花数”并输出。
        // “水仙花数”是指一个三位数，其各位数字的立方和确好等于该数本身.
        // 如；153＝1^3＋5^3＋3^3，则153是一个“水仙花数“。
        for (int i = 100; i < 1000; i++) {
            if(i==(i%10)*(i%10)*(i%10)+(i/10%10)*(i/10%10)*(i/10%10)+(i/100)*(i/100)*(i/100)){
                System.out.println(i);
            }
        }
    }
}
