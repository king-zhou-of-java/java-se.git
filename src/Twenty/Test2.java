package Twenty;

/**
 * User:langchen
 * Date:2022-08-22
 * Time:22:26
 */
public class Test2 {
    public static void func(){

    }
    public static void func(int a){

    }
    public static void func(int a,int b){

    }
    public static void main(String[] args) {
        func();
        func(6);
        func(6,8);
    }
}
