package Twenty;

/**
 * User:langchen
 * Date:2022-08-22
 * Time:21:41
 */
class Animal{
    public String name;
    public int age;
    public Animal(String name,int age){
        this.age = age;
        this.name = name;
    }
    public void eat(){
        System.out.println(name+"很听话");
    }
}

class Dog extends Animal{
    public String color;
    public Dog(String name,int age){
        super(name,age);
        this.color = color;
    }
//    @Override
//    public void eat() {
//        System.out.println(name+"吃骨头");
//    }

    public void bark(){
        System.out.println(name+"汪汪汪~");
    }
}

class Cat extends Animal{
    public Cat(String name,int age) {
        super(name,age);
    }

    public void eat(){
        System.out.println(name+"吃鱼");
    }
    public void mew(){
        System.out.println(name+"喵喵喵~");
    }
}
public class Test1 {
    public static void func(Animal animal){
        animal.eat();
    }
    public static void main(String[] args) {
        Dog dog = new Dog("旺财",6);
        Animal animal = dog;//animal这个引用指向了Dog对象。
        Cat cat = new Cat("咪咪",7);

       if(animal instanceof Cat){
           cat = (Cat) animal;
           cat.mew();
       }
       if(animal instanceof  Dog){
           dog = (Dog) animal;
           dog.bark();
       }
    }
}
