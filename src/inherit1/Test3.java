package inherit1;

/**
 * User:langchen
 * Date:2022-08-20
 * Time:18:50
 */
class A1{
    public String name;
    public int age;
    public A1(String name,int age){//此时父类构造方法是带有两个参数的
        this.age = age;
        this.name = name;
    }
}

class B1 extends A1{
    public String color;
    public B1(String name,int age){//子类构造方法必须带有明确的基类的参数，然后通过super关键字访问父类成员变量
        super("张三丰",18);//必须卸载构造方法第一行
//        this("林青霞",20);
    }
}


public class Test3 {
}
