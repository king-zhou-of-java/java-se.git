package inherit1;

/**
 * User:langchen
 * Date:2022-08-20
 * Time:17:48
 */
class A{
    public String name;
    public  int age;
    public void method1(){
        System.out.println("这是A的method1方法");
    }
    public void method2(){
        System.out.println("这是A的method2方法");
    }
}


class B extends A{
    public String name;// 与父类中成员变量同名且类型相同
    public double age; // 与父类中成员变量同名但类型不同
    public void method1(int a){
        System.out.println("这是B中的method1方法");
    }
    public void method3(){
        name = "张三";// 对于同名的成员变量，直接访问时，访问的都是子类的
        age = 18;
        super.name = "林青霞";// 访问父类的成员变量时，需要借助super关键字
        super.age = 20; // super是获取到子类对象中从基类继承下来的部分

        //父类和子类中构成重载的方法，直接可以通过参数列表区分清访问父类还是子类方法
        method1();//访问父类的method1方法
        method1(30);//访问子类的带一个参数的method1方法
    }
}


public class Test2 {
    public static void main(String[] args) {

    }
}
