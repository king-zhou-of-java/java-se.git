package inherit1;

/**
 * User:langchen
 * Date:2022-08-20
 * Time:17:25
 */
 class Animal{
    public String name;
    public int age;
    public void eat(){
        System.out.println(name+"正在吃东西");
    }
}

class Dog extends Animal{
    public void barks(){
        System.out.println(name+"汪汪汪~");
    }
}

class Cat extends Animal{
    public void mew(){
        System.out.println(name+"喵喵喵");
    }
}

public class Test1 {
    public static void main(String[] args) {
        Dog dog = new Dog();
        System.out.println(dog.name);
        System.out.println(dog.age);
        dog.eat();
        dog.barks();
    }
}
