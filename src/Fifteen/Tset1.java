package Fifteen;

/**
 * User:langchen
 * Date:2022-08-09
 * Time:15:38
 */
 interface demo1{
    void draw();
}
class A implements demo1{

    @Override//Alt+回车 可以自动重写这个draw()方法
    public void draw() {
        System.out.println("点击鼠标");
    }
}
class B implements demo1{

    @Override
    public void draw() {
        System.out.println("关闭鼠标");
    }
}
public class Tset1 {
     public static void clink(demo1 T){//接口不能实例化 可以被一个类实现接口中的方法
         T.draw();//博客这里要写到
     }
    public static void main(String[] args) {
        A a1 = new A();
        B b1 =new B();
        clink(a1);
        clink(b1);
        clink(new A());
        clink(new B());
    }
}
