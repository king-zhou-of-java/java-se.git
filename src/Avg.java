/**
 * User:langchen
 * Date:2022-08-01
 * Time:19:49
 */
//实现一个方法 avg, 以数组为参数, 求数组中所有元素的平均值(注意方法的返回值类型).
public class Avg {
    public static double avg(int[] arr){
        int sum = 0;
        for (int i = 0; i < arr.length; i++) {
            sum+=arr[i];
        }
       return (sum*1.0/arr.length);
    }
    public static void main(String[] args) {
        int[] arr = {1,2,3,4,5};
        System.out.println(avg(arr));
    }
}
