import java.util.Scanner;

/**
 * User:langchen
 * Date:2022-07-28
 * Time:15:47
 */
public class everyone {
    public static void main(String[] args) {
        //输出一个整数的每一位，如：123的每一位是3，2，1
        Scanner sc= new Scanner(System.in);
        int n = sc.nextInt();
        System.out.println("n的每一位分别是："+(n/100)+","+(n/10%10)+","+(n%10));
    }
}
