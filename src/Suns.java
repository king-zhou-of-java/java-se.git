import java.util.Arrays;

/**
 * User:langchen
 * Date:2022-08-01
 * Time:20:52
 */
/*输入：nums = [2,7,11,15], target = 9
输出：[0,1]
解释：因为 nums[0] + nums[1] == 9 ，返回 [0, 1] 。*/
public class Suns {
    public static int[] sums(int[] arr, int target) {
        for (int i = 0; i < arr.length; i++) {
            for (int j = i + 1; j < arr.length; j++) {
                if (target - arr[i] == arr[j])
                    return new int[]{i, j};
            }
        }
        return new int[]{-1,-1};
    }


    public static void main(String[] args) {
        int[] ret = sums(new int[]{2,9,11,15},11);
        System.out.println(Arrays.toString(ret));
    }
}
