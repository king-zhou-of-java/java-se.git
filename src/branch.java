import java.util.Scanner;

/**
 * User:langchen
 * Date:2022-07-28
 * Time:14:59
 */
public class branch {
    public static void main(String[] args) {
        //求一个整数，在内存当中存储时，二进制1的个数。
        Scanner sc = new Scanner(System.in);
        int n = sc.nextInt();
        int count = 0;
        int i = 0;
        for (i = 0; i < 32; i++) {
            if(((n >> i) & 1) == 1){
                count++;
            }
        }
        System.out.println("n的二进制中1的个数为："+count);
    }
}
