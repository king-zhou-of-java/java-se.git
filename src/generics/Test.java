package generics;

/**
 * User:langchen
 * Date:2022-09-13
 * Time:9:00
 *
 */
class Myarray<T>{//表示当前类是泛型类
    public T[] obj = (T[])new Object[10];
    public T[] arr = (T[]) new Object[20];
    //把val的值放在pos位置
    public void setval(int pos,T val){
        obj[pos] = val;
    }
    public T getpos(int pos){
        return obj[pos];
    }
}
public class Test {
    public void func(int a){//只能传int型的数据

    }
    public static void main(String[] args) {
        //泛型存在的两个最大的意义：
        //1.存放元素的时候，会帮你进行类型的检查
        //2.取出元素的时候，会自动帮你进行类型转换，不需要再进行类型的强转了。
        //这两步发生在编译的时候 运行的时候是没有泛型的概念的
        Myarray<Integer> my = new Myarray<Integer>();
        my.setval(0,19);
        my.setval(1,4);
        int a = my.getpos(1);
        System.out.println(a);

        Myarray<String> my1 = new Myarray<String>();//后面这个String也可以不写
        my1.setval(1,"helo");
        my1.setval(2,"java");
        String str = my1.getpos(1);
        String str2 = my1.getpos(2);
        System.out.println(str);
        System.out.println(str2);
        //数组在运行时存储和检查类型信息 泛型在编译时检查类型错误
 //        Integer[] ret = (Integer[]) new Object[10];
//        //jvm会做一些改变 把这些数据放到这个数组array中
//        Object[] array  = {1,2,3,4,"hello"};
    }
}
