package generics;

/**
 * User:langchen
 * Date:2022-09-13
 * Time:18:22
 */
class Message<T>{//通配符讲解
    private T name;

    public void setName(T name) {
        this.name = name;
    }
    public T getName(){
        return name;
    }
}
public class Test3 {
    public static void main(String[] args) {
        Message<String> m1 = new Message<>();//这里如果我们定义的泛型变成Integer 那么这里就会报错
        m1.setName("hello java");
        func(m1);
    }
    // 此时使用通配符"?"描述的是它可以接收任意类型.
    public static void func(Message<?> demo){
        //demo.setName("good");//但是由于不确定类型，所以无法修改
        System.out.println(demo.getName());
    }
}
