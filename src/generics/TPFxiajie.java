package generics;

/**
 * User:langchen
 * Date:2022-09-14
 * Time:16:20
 */
class Food{

}
class Fruit extends Food{

}
class apple extends  Fruit{

}
class banana extends Fruit{

}
class Eat<T>{
    private T eat;
    public T getEat(){
        return eat;
    }
    public void setEat(T eat){
        this.eat = eat;
    }
}
public class TPFxiajie {
    public static void main(String[] args) {
        Eat<Food> f1 = new Eat<>();
        f1.setEat(new Food());
        fun(f1);
        Eat<Fruit> f2 = new Eat<>();
        f2.setEat(new Fruit());
        fun(f2);

    }
    public static void fun(Eat<? super Fruit> temp) {
        // 此时可以修改！！添加的是Fruit 或者Fruit的子类
        //通配符的下界，不能进行读取数据，只能写入数据。
        temp.setEat(new Fruit());
        temp.setEat(new banana());
        System.out.println(temp.getEat());
    }
}
