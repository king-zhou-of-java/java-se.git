//package generics;
//
///**
// * User:langchen
// * Date:2022-09-13
// * Time:17:24+
// */
////class 泛型类名称<类型形参 extends 类型边界>{}
////只接受类型边界的子类 或者 类型边界本身
////没有指定类型边界 E，可以视为 E extends Object
//class Testdemo1<E extends Number>{
//    public static <E extends Comparable<E>> E find(E[] array){//泛型方法
//
//    }
//}
//public class Shangjie {
//    public static void main(String[] args) {
//        Testdemo1<Integer> s1 = new Testdemo1<>();
//        Testdemo1<Float> s2 = new Testdemo1<Float>();
//        Testdemo1<Double> s3 = new Testdemo1<Double>();
//    }
//}
