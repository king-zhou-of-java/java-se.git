package generics;

/**
 * User:langchen
 * Date:2022-09-13
 * Time:17:06
 */
//实现一个类 在这个类中有一个方法 用来找到数组中的最大数？
class Search<T extends Comparable<T>>{//这个泛型类要实现Comparable接口
    public T findmax(T[] arr){
        T max = arr[0];
        for (int i = 0; i < arr.length; i++) {
            if(max.compareTo(arr[i])<0){//才能调用这个compareto方法
                max = arr[i];
            }
        }
        return max;
    }
}

public class GetsMax {
    public static void main(String[] args) {
        Search<Integer> s = new Search<>();
         Integer[] array = {1,2,3,4,5};
         s.findmax(array);
    }
}
