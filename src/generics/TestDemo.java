//package generics;
//
///**
// * User:langchen
// * Date:2022-09-14
// * Time:16:04
// */
//class Food{
//
//}
//class Fruit extends Food{
//
//}
//class apple extends  Fruit{
//
//}
//class banana extends Fruit{
//
//}
//class Eat<T>{
//    private T eat;
//    public T getEat(){
//        return eat;
//    }
//    public void setEat(T eat){
//        this.eat = eat;
//    }
//}
//public class TestDemo {
//    public static void main(String[] args) {
//        Eat<apple> app = new Eat<>();//传入的参数必须是Fruit或者Fruit的子类
//        app.setEat(new apple());
//        fun(app);
//        Eat<banana> ban = new Eat<>();
//        ban.setEat(new banana());
//        fun(ban);
//    }
//    //通配符的上界，不能进行写入数据，只能进行读取数据
//    public static void fun(Eat<? extends Fruit> temp){//通配符的上界是Fruit
//        // 此时使用通配符"?"描述的是它可以接收任意类型，但是由于不确定类型，所以无法修改
//        //temp.setMessage(new Banana()); //仍然无法修改！
//        //temp.setMessage(new Apple()); //仍然无法修改！
//        System.out.println(temp.getEat());
//    }
//}
