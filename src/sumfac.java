import java.util.Scanner;

/**
 * User:langchen
 * Date:2022-07-29
 * Time:20:02
 */
public class sumfac {
    public static int fac1(int n){
        int sum = 0;
        int sum1= 1;
        for (int i = 1; i <= n; i++) {
            sum1 *= i;
            sum += sum1;
        }
        return sum;
    }
    public static void main(String[] args) {
        Scanner sc =new Scanner(System.in);
        int n = sc.nextInt();
        System.out.println(fac1(n));
    }
}
