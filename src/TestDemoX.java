/**
 * User:langchen
 * Date:2022-08-30
 * Time:20:48
 */
public class TestDemoX {
    public static void main(String[] args) {
        int a = 10;
        try{
            System.out.println(a/0);
            int[] arr = {1,2,3,4};
            arr = null;
            System.out.println(arr[100]);
        }
        catch (ArithmeticException e){
            e.printStackTrace();
            System.out.println("出现了算术异常！");
        }
        catch(ArrayIndexOutOfBoundsException e){
            e.printStackTrace();
            System.out.println("数组越界异常！");
        }
        catch (NullPointerException e){
            e.printStackTrace();
            System.out.println("空指针异常！");
        }
        catch (Exception e){
            e.printStackTrace();
        }
        System.out.println("执行后续代码！");
    }
}
//    public static void main3(String[] args) {
//        int[] arr1 =null;
//        System.out.println(arr1.length);
//        System.out.prinln();
//    }
//    public static void main2(String[] args) {
//        int[] arr = {1,2,3,4,5};
//        System.out.println(arr[20]);
//    }