import java.util.Scanner;

/**
 * User:langchen
 * Date:2022-07-28
 * Time:16:27
 */
public class binary {
    public static void main(String[] args) {
        //获取一个数二进制序列中所有的偶数位和奇数位， 分别输出二进制序列
        Scanner sc = new Scanner(System.in);
        int n = sc.nextInt();
        for (int i = 30; i >=0 ; i-=2) {
            System.out.print((n>>i)&1);
        }
        System.out.println();
        for (int i = 31; i >0; i-=2) {
            System.out.print((n>>i)&1);
        }
    }
}
