import java.util.Scanner;

/**
 * User:langchen
 * Date:2022-07-29
 * Time:20:24
 */
//求斐波那契数列的第n项
public class Fib {
    public static int Fibo(int n){
        if(n<=2){
            return 1;
        }
        else{
            return Fibo(n-1)+Fibo(n-2);
        }
    }
    public static void main(String[] args) {
        Scanner sc =new Scanner(System.in);
        int n = sc.nextInt();
        System.out.println(Fibo(n));
    }
}
