import java.util.Scanner;

/**
 * User:langchen
 * Date:2022-07-29
 * Time:19:31
 */
/*创建方法求两个数的最大值max2，随后再写一个求3个数的最大值的函数max3。

 要求：在max3这个函数中，调用max2函数，来实现3个数的最大值计算*/
public class Max {
    public static int max2(int x,int y){
        int ret = x>y? x:y;
        return ret;
    }
    public static int max3(int x,int y,int z){
        int sum = max2(x,y);
        int sum1 = sum>z? sum:z;
        return sum1;

    }
    public static void main(String[] args) {
        int a = 1;
        int b = 2;
        int c = 3;
        System.out.println(max2(a,b));
        System.out.println( max3(a,b,c));
    }
}
