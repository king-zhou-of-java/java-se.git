import java.util.Scanner;

/**
 * User:langchen
 * Date:2022-07-31
 * Time:9:07
 */
public class sixlook {
    public  static void fun(int n){
        if(n>=10){
            fun(n/10);
        }
        System.out.print(n%10+" ");
    }
    public static void main(String[] args) {
        Scanner sc= new Scanner(System.in);
        int a = sc.nextInt();
        fun(a);
    }
}
