/**
 * User:langchen
 * Date:2022-08-12
 * Time:23:10
 */
public class Water {
    public static void main(String[] args) {
//        String n1 = "hello bit";// 使用常量串构造
//        String n2  = new String("你好，java");// 直接newString对象
//        char[] arr = {'h','e','l','l','o'};// 使用字符数组进行构造
//        String n3= new String(arr);
//        System.out.println(n1);
//        System.out.println(n2);
//        System.out.println(n3);
//        // 打印"hello"字符串(String对象)的长度
//        System.out.println("hello".length());
//        System.out.println(n1.isEmpty());
        // 对于引用类型变量，==比较两个引用变量引用的是否为同一个对象
//        String s1 = new String("hello");
//        String s2 = new String("hello");
//        String s3 = new String("world");
//        String s4 = s1;
//        System.out.println(s1 == s2); // false
//        System.out.println(s2 == s3); // false
//        System.out.println(s1 == s4); // true
//        System.out.println(s1.equals(s2)); // true
//        System.out.println(s1.equals(s3));// false

//        1. 先按照字典次序大小比较，如果出现不等的字符，直接返回这两个字符的大小差值
//        2. 如果前k个字符相等(k为两个字符长度最小值)，返回值两个字符串长度差值
        String s1 = new String("abc");
        String s2 = new String("ac");
        String s3 = new String("abc");
        String s4 = new String("abcdef");
        System.out.println(s1.compareTo(s2)); // 不同输出字符差值-1
        System.out.println(s1.compareTo(s3)); // 相同输出 0
        System.out.println(s1.compareTo(s4)); // 前k个字符完全相同，输出长度差值 -3
    }
}
