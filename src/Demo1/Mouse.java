package Demo1;

/**
 * User:langchen
 * Date:2022-08-08
 * Time:23:24
 */
//   如果类没有实现接口中的所有的抽象方法，则类必须设置为抽象类
public class Mouse implements USB{
    public void methodA(){
        System.out.println("点击鼠标左键");
    }

    @Override
    public void methodB() {
        System.out.println("点击鼠标右键");
    }

    @Override
    public void methodC() {
        System.out.println("关闭鼠标");
    }
}
