package Demo1;

/**
 * User:langchen
 * Date:2022-08-08
 * Time:23:19
 */
public interface USB {
    public abstract void methodA();//public abstract是固定搭配，可以不写
    public void methodB();
    void methodC();//注意：在接口中上述写法都是抽象方法,推荐这种方式，代码更简洁
    String brand = "mi";
//   public USB(){
//
//    }
//接口中不能有静态代码块和构造方法
}

