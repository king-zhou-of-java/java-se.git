/**
 * User:langchen
 * Date:2022-08-01
 * Time:21:23
 */
//给定一个非空整数数组，除了某个元素只出现一次以外，其余每个元素均出现两次。找出那个只出现了一次的元素。
public class GetArray {
    public static int getarr(int[]arr){
        for (int i = 0; i < arr.length; i++) {
            for (int j = i+1; j < arr.length; j++) {
                if(arr[i]!=arr[j]){
                    return arr[i];
                }
            }
        }
        return -1;
    }
    public static void main(String[] args) {
        int[] arr = {1,2,2,3,3,4,4};
        int ret = getarr(arr);
        System.out.println(ret);
    }
}
