import java.util.InputMismatchException;
import java.util.Scanner;

/**
 * User:langchen
 * Date:2022-08-30
 * Time:22:35
 */
public class Testdemo3 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        try{
            int n = sc.nextInt();
        }catch (InputMismatchException e){
            e.printStackTrace();
            System.out.println("输入数据不匹配！");
        }
        finally {
            System.out.println("执行了finally部分，一般用来关闭资源！");
            sc.close();
        }
    }
}
