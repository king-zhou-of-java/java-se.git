import java.util.Scanner;

/**
 * User:langchen
 * Date:2022-07-28
 * Time:15:52
 */
public class imitate{
    public static void main(String[] args) {
        //编写代码模拟三次密码输入的场景。
        // 最多能输入三次密码，密码正确，提示“登录成功”,密码错误，
        // 可以重新输入，最多输入三次。三次均错，则提示退出程序
        Scanner sc= new Scanner(System.in);
        int secret = 123456;
        for (int i = 0; i < 3; i++) {
                String name = sc.nextLine();
                if(name.equals(secret)){
                    System.out.println("密码错误，请重新输入");
                }
                else{
                    System.out.println("密码正确，登录成功！");
                    break;
                }
            }
        }
}
