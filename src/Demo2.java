/**
 * User:langchen
 * Date:2022-08-08
 * Time:21:04
 */
// 抽象类：被abstract修饰的类
public abstract class Demo2 {
    abstract public void eat();
    public String name;
    public String color;
    abstract class Cat extends Demo2{
   /*     public void eat(){
            System.out.println(name+"猫吃鱼");
        }*/
    }
    public static void main(String[] args) {
    //Demo2 de = new Demo2();//抽象类不能直接实例化对象
    }
}
//    abstract private void nice();//抽象方法不能是private的
//    abstract public final void methodA();//抽象方法不能被final static 修饰
// abstract public void drink();
// 抽象类也是类，也可以增加普通方法和属性